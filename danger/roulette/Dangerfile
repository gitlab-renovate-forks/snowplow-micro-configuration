PROJECT_NAME = helper.config.project_name

MESSAGE = <<MARKDOWN
## Reviewer roulette

Changes that require review have been detected! A merge request is normally
reviewed and merged by a maintainer for this repository.
MARKDOWN

TABLE_MARKDOWN = <<MARKDOWN

To spread load more evenly across eligible maintainer, Danger has picked a candidate.
[Override these selections](https://about.gitlab.com/handbook/engineering/projects/##{PROJECT_NAME})
if you think someone else would be better-suited.

To read more on how to use the reviewer roulette, please take a look at the
[Engineering workflow](https://about.gitlab.com/handbook/engineering/workflow/#basics)
and [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).

Once you've decided who will review this merge request, mention them as you
normally would! Danger does not automatically notify them for you.

MARKDOWN

TABLE_HEADER = <<MARKDOWN
| Maintainer |
| ---------- |
MARKDOWN

WARNING_MESSAGE = <<MARKDOWN
⚠ Failed to retrieve information ⚠
%{warnings}
MARKDOWN

NOT_AVAILABLE_TEMPLATE = 'No %{role} available'

def note_for_spin(spin, role)
  note = spin.public_send(role)&.markdown_name(author: roulette.team_mr_author)
  return note if note

  NOT_AVAILABLE_TEMPLATE % { role: role }
end

def markdown_row_for_spin(spin)
  maintainer_note = note_for_spin(spin, :maintainer)

  row = +"| #{maintainer_note} |"
  row
end

def warning_list(roulette)
  return unless roulette.warnings.any?

  roulette.warnings.map { |warning| "* #{warning}" }.join("\n")
end

if helper.changes.any?
  random_roulette_spins = roulette.spin(nil)

  rows = random_roulette_spins.map do |spin|
    markdown_row_for_spin(spin)
  end

  markdown(MESSAGE)

  warnings = warning_list(roulette)

  markdown(format(WARNING_MESSAGE, warnings: warnings)) if warnings

  markdown(TABLE_MARKDOWN + TABLE_HEADER + rows.join("\n")) unless rows.empty?
end

